FROM alpine:latest
RUN apk update && \
    apk add --no-cache \
    lighttpd \
    curl && \
    rm -rf /var/cache/apk/*
EXPOSE 80
CMD ["/usr/sbin/lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]